package com.org.pnv.petshopinmobile.activity;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.org.pnv.petshopinmobile.App.AppController;
import com.org.pnv.petshopinmobile.R;
import com.org.pnv.petshopinmobile.adapter.ProductAdapter;
import com.org.pnv.petshopinmobile.model.Product;
import com.org.pnv.petshopinmobile.utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ProductActivity extends BaseActivity implements SearchView.OnQueryTextListener {

    // url to get all titles list
    private static String url_all_products = Common.API_SERVER_IP + "api/ApiController";
    // Log tag
    private static final String TAG = ProductActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    private List<Product> productList = new ArrayList<Product>();
    private ListView listView;
    private ProductAdapter adapter;
    SearchView editsearch;
    Button btnPricemenu;
    private int value_price_1 = 0;
    private int value_price_2 = 0;
    private ProductAdapter adapter1;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_department);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        listView = (ListView) findViewById(R.id.lv_department);
        adapter = new ProductAdapter(this, productList);
        listView.setAdapter(adapter);

        pDialog = new ProgressDialog(this);
        // Showing progress dialog before making http request
        pDialog.setMessage("Loading...");
        pDialog.show();

        // changing action bar color
        getSupportActionBar().setBackgroundDrawable(
                new ColorDrawable(Color.parseColor("#1b1b1b")));

        // Creating volley request obj
        JsonArrayRequest departReq = new JsonArrayRequest(url_all_products,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        hidePDialog();

                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Product department = new Product();
                                department.setProduct_id(obj.getInt("product_id"));
                                department.setProduct_name(obj.getString("product_name"));
                                department.setCategory_id(obj.getLong("category_id"));
                                department.setProduct_price(obj.getDouble("product_price"));
                                department.setProduct_description( obj.getString("product_description"));
                                department.setImageUrl(Common.SERVER_DEPARTMENT_IMAGE_RESOURCE + obj.getString("product_image"));
                                Log.v("Image:     ", Common.SERVER_DEPARTMENT_IMAGE_RESOURCE + obj.getString("product_image"));


                                // adding movie to movies array
                                productList.add(department);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        adapter.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hidePDialog();

            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(departReq);

        // Locate the EditText in listview_main.xml
        editsearch = (SearchView) findViewById(R.id.search);
        editsearch.setOnQueryTextListener(this);

        btnPricemenu = (Button) findViewById(R.id.btnPriceMenu);
        //Đăng ký ContextMenu cho button
        registerForContextMenu(btnPricemenu);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        // code of dung :))
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(ProductActivity.this, "selected", Toast.LENGTH_SHORT).show();
                Intent nextScreen = new Intent(getApplicationContext(), DetailProductActivity.class);
                nextScreen.putExtra("Product",(Product)listView.getItemAtPosition(position));

                // chua xu ly so 30
                startActivityForResult(nextScreen,30);
            }
        });
    }

    //Nạp contextmenu mà chúng ta vừa tạo vào ứng dụng
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu_layout, menu);
    }

    //Xử lý sự kiện khi click vào từng item
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        String message = null;
        switch (item.getItemId()) {
            case R.id.Under100:
                value_price_1 = 0;
                value_price_2 = 100000;
                message = "Products have price under 100.000";
                break;

            case R.id.From100To300:
                value_price_1 = 100000;
                value_price_2 = 300000;
                message = "Products have price from 100.000 to 300.000";
                break;
            case R.id.From300To500:
                value_price_1 = 300000;
                value_price_2 = 500000;
                message = "Products have price from 300.000 to 500.000";
                break;
            case R.id.More500:
                value_price_1 = 500000;
                message = "Products have price more than 500.000";
                break;
        }

        adapter.filterByPrice(value_price_1, value_price_2);
        adapter1 = new ProductAdapter(this, productList);
        listView.setAdapter(adapter1);
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        return super.onContextItemSelected(item);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode) {

    }

    //menu on tab bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            Intent i1 = new Intent(getApplicationContext(), LogInActivity.class);
            startActivity(i1);
            finish();
        }

        if (id == R.id.setting) {
            //insert more code
        }

        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {

          this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        String text = s;
        adapter.filter(text);

        return false;
    }




    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Department Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

}
