package com.org.pnv.petshopinmobile.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.org.pnv.petshopinmobile.R;
import com.org.pnv.petshopinmobile.model.Product;

/**
 * Created by dung.nguyen on 5/5/2017.
 */

public class DetailProductActivity extends BaseActivity {
    private TextView productName;
    private TextView productPrice;
    private TextView productDescription;
    private Product product;
    private NetworkImageView mNetworkImageView;
    private ImageLoader mImageLoader;
    private RequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        productName = (TextView) findViewById(R.id.tvProductName);
        productPrice = (TextView) findViewById(R.id.tvProductPrice);
        productDescription = (TextView) findViewById(R.id.tvDescription);
        mNetworkImageView = (NetworkImageView) findViewById(R.id.networkImageView);

        product = (Product) getIntent().getExtras().get("Product");
        productName.setText(product.getProduct_name());
        productPrice.setText(String.valueOf(product.getProduct_price()));
        productDescription.setText(product.getProduct_description());

    }

    @Override
    protected void onStart() {
        super.onStart();
        mRequestQueue = Volley.newRequestQueue(this.getApplicationContext());
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);
            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
        mNetworkImageView.setImageUrl(product.getImageUrl(), mImageLoader);
    }



    @Override
    public void onPermissionsGranted(int requestCode) {

    }
    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.org.pnv.petshopinmobile.R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == com.org.pnv.petshopinmobile.R.id.logout) {
            Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
            startActivity(intent);
            finish();
        }


        if (id == com.org.pnv.petshopinmobile.R.id.setting){
            //insert more code
        }

        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {

            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
