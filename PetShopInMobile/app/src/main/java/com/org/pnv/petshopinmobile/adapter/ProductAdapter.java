package com.org.pnv.petshopinmobile.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.org.pnv.petshopinmobile.App.AppController;
import com.org.pnv.petshopinmobile.R;
import com.org.pnv.petshopinmobile.model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Laptop033 on 2/22/2017.
 */

public class ProductAdapter extends BaseAdapter{

    private Activity activity;
    private LayoutInflater inflater;
    private List<Product> productList;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private ArrayList<Product> arraylist = new ArrayList<Product>();

    public ProductAdapter(Activity activity, List<Product> productList) {
        this.activity = activity;
        this.productList = productList;
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int i) {
        return productList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }



    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null)
            view = inflater.inflate(R.layout.list_department_row, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) view
                .findViewById(R.id.thumbnail);
        TextView title = (TextView) view.findViewById(R.id.title);
        TextView depCode = (TextView) view.findViewById(R.id.depCode);
        TextView description = (TextView) view.findViewById(R.id.description);
        TextView total_staff = (TextView) view.findViewById(R.id.total_staff);

        // getting movie data for the row
        Product d = productList.get(position);

        // thumbnail image
        thumbNail.setImageUrl(d.getImageUrl(), imageLoader);

        // Name
        title.setText(d.getProduct_name());

        // Code
        depCode.setText("Code: " + String.valueOf(d.getProduct_id()));

        // description
        description.setText(d.getProduct_description());

        // release year
        total_staff.setText(String.valueOf(5));

        return view;
    }

    public void filter(String charText) {
        if(arraylist.size()==0) {
            this.arraylist.addAll(productList);
        }
        String lowerText = charText.toLowerCase(Locale.getDefault());

        productList.clear();
        if (charText.length() == 0) {
            productList.addAll(arraylist);
        } else {
            for (Product wp : arraylist) {
                if (wp.getProduct_name().toLowerCase(Locale.getDefault()).contains(lowerText)) {
                    productList.add(wp);
                }
            }
        }

        notifyDataSetChanged();
    }

    public void filterByPrice(int value_price_1, int value_price_2) {
        this.arraylist.addAll(productList);
        productList.clear();
        if (value_price_1 == 500000) {
            for (Product wp : arraylist) {
              if(wp.getProduct_price()>= value_price_1){
                  productList.add(wp);
              }
            }
        }

        else{
            for (Product wp: arraylist){
                if ((wp.getProduct_price() >= value_price_1) && (wp.getProduct_price() <= value_price_2)) {
                    productList.add(wp);
                }
            }
        }
    }
}
