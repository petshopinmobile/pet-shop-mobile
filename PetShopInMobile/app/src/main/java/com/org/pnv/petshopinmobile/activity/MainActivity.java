package com.org.pnv.petshopinmobile.activity;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.org.pnv.petshopinmobile.R;


public class MainActivity extends BaseActivity {

    Button btnViewTitles;
    Button btnNewTitles;
    Button btnViewDepartments;
    Button btnCreateDepartment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Buttons
        btnViewTitles = (Button) findViewById(R.id.btnViewProducts);
        btnNewTitles = (Button) findViewById(R.id.btnCreateProduct);
        btnViewDepartments = (Button) findViewById(R.id.btnViewDepartments);
        btnCreateDepartment = (Button) findViewById(R.id.btnCreateDepartment);

        // view titles click event
        btnViewTitles.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // Launching All titles Activity
                Intent i = new Intent(getApplicationContext(), AllTitlesActivity.class);
                startActivity(i);

            }
        });

        // view titles click event
//        btnNewTitles.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                // Launching create new product activity
//                Intent i = new Intent(getApplicationContext(), NewTitleActivity.class);
//                startActivity(i);
//
//            }
//        });

        btnViewDepartments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // Launching create new product activity
                Intent i = new Intent(getApplicationContext(), ProductActivity.class);
                startActivity(i);

            }
        });
        btnCreateDepartment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // Launching create new product activity
                Intent i = new Intent(getApplicationContext(), AddNewDepartmentActivity.class);
                startActivity(i);

            }
        });
    }

    @Override
    public void onPermissionsGranted(int requestCode) {

    }

    //menu on tab bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            Intent i1 = new Intent(getApplicationContext(), LogInActivity.class);
            startActivity(i1);
            finish();
        }

        if (id == R.id.setting){
            //insert more code
        }

        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {

            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
