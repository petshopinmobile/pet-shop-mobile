package com.org.pnv.petshopinmobile.model;
// Generated Apr 24, 2015 4:37:16 PM by Hibernate Tools 4.3.1


import java.io.Serializable;

/**
 * Departments generated by hbm2java
 */

public class Product implements Serializable {


    private long product_id;
    private long category_id;
    private String product_name;
    private String product_image;
    private double product_price;
    private String product_description;


    public Product() {
    }

    public Product(Long product_id, Long category_id, String product_name, String product_image, double product_price, String product_description) {
        this.product_id = product_id;
        this.category_id = category_id;
        this.product_name = product_name;
        this.product_image = product_image;
        this.product_price = product_price;
        this.product_description = product_description;
    }

    public Product(String product_name, double product_price, String product_description) {
        this.product_name = product_name;
        this.product_price = product_price;
        this.product_description = product_description;

    }

    public long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(long product_id) {
        this.product_id = product_id;
    }

    public long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(long category_id) {
        this.category_id = category_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }



    public double getProduct_price() {
        return product_price;
    }

    public void setProduct_price(double product_price) {
        this.product_price = product_price;
    }

    public String getProduct_description() {
        return product_description;
    }

    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    public String getImageUrl() {
        return product_image;
    }

    public void setImageUrl(String imageUrl) {
        this.product_image = imageUrl;
    }

    @Override
    public String toString() {
        return "Product{" +
                "product_id=" + product_id +
                ", category_id=" + category_id +
                ", product_name='" + product_name + '\'' +
                ", product_image='" + product_image + '\'' +
                ", product_price=" + product_price +
                ", product_description='" + product_description + '\'' +
                '}';
    }
}
