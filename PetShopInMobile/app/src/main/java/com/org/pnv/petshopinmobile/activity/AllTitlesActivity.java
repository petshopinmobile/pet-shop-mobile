package com.org.pnv.petshopinmobile.activity;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.org.pnv.petshopinmobile.R;
import com.org.pnv.petshopinmobile.model.Titles;
import com.org.pnv.petshopinmobile.utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class AllTitlesActivity extends BaseActivity {


    // Progress Dialog
    private ProgressDialog pDialog;
    private ListView lv;

    ArrayList<HashMap<String, String>> titleList;

    // url to get all titles list
    private static String url_all_titles = Common.API_SERVER_IP+"api/title";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "titles";
    private static final String TAG_TID = "tid";
    private static final String TAG_NAME = "titleName";
    private static final String TAG_DES = "description";
    private static final String TAG_CODE = "titleCode";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_titles);

        // Hashmap for ListView
        titleList = new ArrayList<HashMap<String, String>>();

        // Loading products in Background Thread
        new LoadAllTitles().execute();

        // Get listview
         lv =  (ListView) findViewById(R.id.lv_title) ;//getListView();

        // on seleting single product
        // launching Edit Product Screen
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                String pid = ((TextView) view.findViewById(R.id.pid)).getText()
                        .toString();

                // Starting new intent
//                Intent in = new Intent(getApplicationContext(),
//                        EditTitleActivity.class);
//                // sending pid to next activity
//                in.putExtra(TAG_TID, pid);
//
//                // starting new activity and expecting some response back
//                startActivityForResult(in, 100);
            }
        });
    }

    @Override
    public void onPermissionsGranted(int requestCode) {

    }

    private void setListAdapter(ListAdapter adapter){
        this.lv.setAdapter(adapter);
    }

    // Response from Edit Product Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // if result code 100
        if (resultCode == 100) {
            // if result code 100 is received
            // means user edited/deleted product
            // reload this screen again
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

    }

    /**
     * Background Async Task to Load all product by making HTTP Request
     * */
    class LoadAllTitles extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AllTitlesActivity.this);
            pDialog.setMessage("Loading titles. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        /**
         * getting All titles from url
         * */
        protected String doInBackground(String... args) {
            try {
                URL url = new URL(url_all_titles);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                }
                finally{
                    urlConnection.disconnect();
                }
            }
            catch(Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String response) {
            // dismiss the dialog after getting all titles
            pDialog.dismiss();


            /****************** Start Parse Response JSON Data *************/

            String OutputData = "";

            try {


                /***** Returns the value mapped by name if it exists and is a JSONArray. ***/
                /*******  Returns null otherwise.  *******/
                JSONArray jsonMainNode = new JSONArray(response);

                /*********** Process each JSON Node ************/

                int lengthJsonArr = jsonMainNode.length();
                String id,titleCode,titleName, description;
                Titles title;
                for(int i=0; i < lengthJsonArr; i++)
                {
                    /****** Get Object for each JSON node.***********/
                    JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);

                    /******* Fetch node values **********/
                         id       = jsonChildNode.optString("id").toString();
//                     titleCode     = jsonChildNode.optString("titleCode").toString();
                        titleName = jsonChildNode.optString("titleName").toString();
//                     description = jsonChildNode.optString("description").toString();
//                    title = new Titles( Integer.getInteger(id) ,titleCode, titleName, description ) ;

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    map.put(TAG_TID, id);
                    map.put(TAG_NAME, titleName);
                    titleList.add(map);

                }
                /****************** End Parse Response JSON Data *************/

            } catch (JSONException e) {

                e.printStackTrace();
            }

                // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            AllTitlesActivity.this, titleList,
                            R.layout.list_title_item, new String[] { TAG_TID,
                            TAG_NAME},
                            new int[] { R.id.pid, R.id.name });
                    // updating listview
                    setListAdapter(adapter);
                }
            });

        }

    }

    //menu on tab bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            Intent i1 = new Intent(getApplicationContext(), LogInActivity.class);
            startActivity(i1);
            finish();
        }

        if (id == R.id.setting){
            //insert more code
        }

        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {

            //This method is called when the up button is pressed. Just the pop back stack.
            FragmentManager fm = getFragmentManager();
            if (fm.getBackStackEntryCount() > 0) {
                fm.popBackStack();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
