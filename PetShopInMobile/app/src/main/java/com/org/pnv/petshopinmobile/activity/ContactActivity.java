package com.org.pnv.petshopinmobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Created by vien.le on 5/8/2017.
 */

public class ContactActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(com.org.pnv.petshopinmobile.R.layout.contact);
    }
    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.org.pnv.petshopinmobile.R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == com.org.pnv.petshopinmobile.R.id.logout) {
            Intent intent = new Intent(getApplicationContext(), LogInActivity.class);
            startActivity(intent);
            finish();
        }


        if (id == com.org.pnv.petshopinmobile.R.id.setting){
            //insert more code
        }

        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {

            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
