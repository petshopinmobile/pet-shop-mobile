package com.org.pnv.petshopinmobile.activity;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.org.pnv.petshopinmobile.App.AppController;
import com.org.pnv.petshopinmobile.R;
import com.org.pnv.petshopinmobile.model.User;
import com.org.pnv.petshopinmobile.utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by vien.le on 4/21/2017.
 */

public class RegisterActivity extends AppCompatActivity {

    private static String url_register = Common.API_SERVER_IP+"api/users/register";
    private String realPath = "";
    private static final String KEY_USER_EMAIL = "email";
    private static final String KEY_USER_PASSWORD = "pass";
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    private User users;

    Button btComeLogin;
    Button btRegis;

    EditText edEmail;
    EditText edPass;
    EditText edConfirmPass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        edEmail = (EditText) findViewById(R.id.email);
        edPass = (EditText) findViewById(R.id.password);
        edConfirmPass = (EditText) findViewById(R.id.edConfirmPass);

        btRegis = (Button) findViewById(R.id.btRegis);
        btComeLogin = (Button) findViewById(R.id.btComeLogin);

        btRegis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkRegister();
            }
        });

        btComeLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(i);
            }
        });
    }

    private void checkRegister(){

        String email = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+";

        if(edEmail.getText().toString().length() == 0){
            edEmail.setError("Email not entered!");
            edEmail.requestFocus();
        }
        else if (!edEmail.getText().toString().contains("@") || !edEmail.getText().toString().contains(".")){
            edEmail.setError("Please check your email! ");
            edEmail.requestFocus();
        }
        else if(!edEmail.getText().toString().matches(email)){
            edEmail.setError("Email don't exist Special charactors");
            edEmail.requestFocus();
        }
        else if(edPass.getText().toString().length() == 0){
            edPass.setError("Password not entered!");
            edPass.requestFocus();
        }
        else if(edConfirmPass.getText().toString().length() == 0){
            edConfirmPass.setError("Please confirm password");
            edConfirmPass.requestFocus();
        }
        else if(!edPass.getText().toString().equals(edConfirmPass.getText().toString())){
            edConfirmPass.setError("Password not match!");
            edConfirmPass.requestFocus();
        }
        else if(edPass.getText().toString().length() < 3  ){
            edPass.setError("Password should be atleast of 3 charactors!");
            edPass.requestFocus();
        }
        else{
//            Toast.makeText(getApplicationContext(),"Successfully!", Toast.LENGTH_LONG).show();
            executeRegisterToServer();
        }
    }

    private void executeRegisterToServer() {
        final ProgressDialog loading = ProgressDialog.show(this,"Uploading...","Please wait...",false,false);
            Log.d("url_register",url_register);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_register,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        loading.dismiss();

                        String thongbao =  "Sai";
                        JSONObject responseStatus = null;
                        try {
                            responseStatus = new JSONObject(s);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        int status = 0;
                        try {
                            status = responseStatus.getInt("status");

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                        if (status == 0) {
                            thongbao = "Check email or password again!";
                        }
                        else {
                            thongbao = "Successfully!";
                            Intent intent = new Intent(getApplicationContext(), HomePageActivity.class);
                            startActivity(intent);
                        }
                        //Showing toast message of the response
                        Toast.makeText(RegisterActivity.this,thongbao,
                                Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        Log.d(TAG, "Error: " + volleyError.getMessage());
                        //Showing toast
                        Toast.makeText(RegisterActivity.this, volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            //Converting Bitmap to String
                            File file = new File(realPath);
                            String fileName = file.getName();

                            //Getting Email and Password
                            String email = edEmail.getText().toString().trim();
                            String password = edPass.getText().toString().trim();

                            //Creating parameters
                            Map<String,String> params = new Hashtable<String, String>();

                            //Adding parameters
                            params.put(KEY_USER_EMAIL, email);
                            params.put(KEY_USER_PASSWORD, password);

                            //returning parameters
                            return params;
                        }
                    };
        //Adding request to the queue
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    //menu on tab bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            Intent i1 = new Intent(getApplicationContext(), LogInActivity.class);
            startActivity(i1);
            finish();
        }

        if (id == R.id.setting){
            //insert more code
        }

        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }
}


