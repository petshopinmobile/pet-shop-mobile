package com.org.pnv.petshopinmobile.activity;

import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.org.pnv.petshopinmobile.App.AppController;
import com.org.pnv.petshopinmobile.R;
import com.org.pnv.petshopinmobile.model.User;
import com.org.pnv.petshopinmobile.utils.Common;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Hashtable;
import java.util.Map;


public class LogInActivity extends AppCompatActivity{

    Button btSignup;

    private static String url_logIn = Common.API_SERVER_IP+"api/users/login";
    private String realPath = "";
    AutoCompleteTextView emailText;
    EditText passwordText;
    private static final String KEY_USER_EMAIL = "email";
    private static final String KEY_USER_PASSWORD = "pass";
   // private static final String KEY_USER_ROLE = "user_role";
    private static final String TAG = LogInActivity.class.getSimpleName();
    private ProgressDialog pDialog;
    private User users;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btSignup = (Button) findViewById(R.id.signup);
        btSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });

        emailText= (AutoCompleteTextView) findViewById(R.id.email);
        passwordText= (EditText) findViewById(R.id.password);

        Button loginButton = (Button) findViewById(R.id.logIn_Button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initLogin();
            }
        });

    }

    private void initLogin() {
        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if(email.length() == 0){
            emailText.setError("Please enter email!");
            emailText.requestFocus();
        }
        else if(!email.contains("@") || !email.contains(".")){
            emailText.setError("Please check your email! ");
            emailText.requestFocus();
        }
        else if(password.length() == 0){
            passwordText.setError("Please enter password!");
            passwordText.requestFocus();
        }
        else if (!email.matches(emailPattern)){
            emailText.setError("Don't allow special characters!");
            emailText.requestFocus();
        }
        else if(password.length() <3  ){
            passwordText.setError("Incorrect Password! ");
            passwordText.requestFocus();
        }

        else{
            executeLogInToServer();
        }

    }

    private void executeLogInToServer() {
        final ProgressDialog loading = ProgressDialog.show(this,"Uploading...","Please wait...",false,false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_logIn,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        loading.dismiss();

                      // String thongbao = "ngu";
                        JSONObject responseStatus = null;
                        try {
                            responseStatus = new JSONObject(s);
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }

                        int status = 0;
                        try {
                            status = responseStatus.getInt("status");

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                        if (status == 0) {
                            System.out.println("Eror email or password");
                        } else {
                            //thong bao = checkAdminOrNot(responseStatus);
                            checkAdminOrNot(responseStatus);
                        }
                        //Showing toast message of the response
                        //thong bao
                        Toast.makeText(LogInActivity.this,"Suceesfully!",
                                Toast.LENGTH_LONG).show();

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();



                        //Showing toast
                        Toast.makeText(LogInActivity.this, volleyError.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String


                File file = new File(realPath);
                String fileName = file.getName();

                //Getting Image Name
                String email = emailText.getText().toString().trim();
                String password = passwordText.getText().toString().trim();

                //Creating parameters
                Map<String,String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put(KEY_USER_EMAIL, email);
                params.put(KEY_USER_PASSWORD, password);

                //returning parameters
                return params;
            }
        };



        //Adding request to the queue
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public void checkAdminOrNot(JSONObject responseStatus) {
        //String thongbao = "";

        try {
            JSONObject user = responseStatus.getJSONObject("dataObject");
           boolean role = user.getBoolean("user_role");

            if (role == true) {
                Intent intent = new Intent(getApplicationContext(), HomePageActivity.class);
                startActivity(intent);

            } else if (role == false) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                //thongbao = "Hello admin";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
          //  return thongbao;
    }


    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            Intent i1 = new Intent(getApplicationContext(), LogInActivity.class);
            startActivity(i1);
            finish();
        }

        if (id == R.id.setting){
            //insert more code
        }

        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {

            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }
}

